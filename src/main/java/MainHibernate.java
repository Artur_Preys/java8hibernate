import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import domain.Department;
import domain.Employee;
import domain.Project;
import repository.DepartmentHiberRepository;
import repository.EmployeeHiberRepository;
import repository.ProjectHiberRepository;
import utils.HibernateUtils;

public class MainHibernate {

    public static void main(String[] args) {
        EmployeeHiberRepository employeeHiberRepository = new EmployeeHiberRepository();
       /* Employee employee = new Employee();
        employee.setFirstName("Test");
        employee.setLastName("TestLastname");
        employee.setDateOfBirth(new Date());
        employee.setEmail("test@gmail.com");
        employee.setSalary(1222);

        employeeHiberRepository.save(employee);

        List<Employee> employees = employeeHiberRepository.findAll();
        System.out.println(employees.size());

        employees.forEach(it-> System.out.println(it));
*/

        employeeHiberRepository.findById(1);

        DepartmentHiberRepository departmentHiberRepository = new DepartmentHiberRepository();
        List<Department> dep = departmentHiberRepository.findAll();
        dep.forEach(System.out::println);

        List<Employee> employeesFromHr = employeeHiberRepository.findByDepartment("HR");
        employeesFromHr.forEach(System.out::println);


        List<Employee> employeesbyNameStarts = employeeHiberRepository.findAllByNameStartsWith("J");
        employeesbyNameStarts.forEach(System.out::println);


        List<Employee> sortedEmployees = employeeHiberRepository.findAllSortedASC();
        sortedEmployees.forEach(System.out::println);

        System.out.println("");

        List<Department> listByCriteria = departmentHiberRepository.findAllByCriteria();

        listByCriteria.forEach(System.out::println);


        List<Department> listByName = departmentHiberRepository.findAllByName("Finance");

        listByName.forEach(System.out::println);



        List<Department> listByNameOrID = departmentHiberRepository.findAllByNameOrId("Finance", 3);
        listByNameOrID.forEach(System.out::println);



        List<Employee> employeesByDept = employeeHiberRepository.findByDepartmentByCriteria("HR");

        employeesByDept.forEach(System.out::println);


        List<Employee> sortedByCriteria = employeeHiberRepository.findAllSortedByCr();
        sortedByCriteria.forEach(System.out::println);




    }
}
