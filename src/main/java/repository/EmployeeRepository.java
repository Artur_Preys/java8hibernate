package repository;

import static utils.DatabaseUtils.DATABASE_HOST;
import static utils.DatabaseUtils.DATABASE_PASSWORD;
import static utils.DatabaseUtils.DATABASE_USERNAME;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EmployeeRepository {

    String insertIntoEmployees = "INSERT INTO employees (firstname,lastname, dateOfBirth, phoneNumber, email, salary) values ("
        + "'Matt', "
        + "'Matthews', "
        + "'1980-01-01', "
        + "'0800-800-800', "
        + "'m@matthews@gmail.com', "
        + "'3000')";

    String insertIntoEmployeesPrepared = "INSERT INTO employees (firstname,lastname, dateOfBirth, phoneNumber, email, salary) values (?, ? , ? ,? , ? , ?)";

    public void createEmployeeOld() {
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD)) {
            Statement stmt = conn.createStatement();
            Integer affectedRows = stmt.executeUpdate(insertIntoEmployees);
            System.out.println("affected rows: " + affectedRows);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void createEmployee() {
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD)) {
            PreparedStatement stmt = conn.prepareStatement(insertIntoEmployeesPrepared);
            stmt.setString(1, "Matt2");
            stmt.setString(2, "Matthews2");
            stmt.setDate(3, Date.valueOf("1981-01-01"));
            stmt.setString(4, "08008080-0808");
            stmt.setString(5, "mat.mat@gmail.com");
            stmt.setInt(6, 3100);
            Integer affectedRows = stmt.executeUpdate();
            System.out.println("affected rows: " + affectedRows);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void findAll() {
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD)) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("Select * from employees");

            while (rs.next()) {
                Integer empId = rs.getInt("employeeId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                System.out.println(empId + " " + firstName + " " + lastName);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
