package repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import domain.Employee;
import utils.HibernateUtils;

public class EmployeeHiberRepository {

    public Employee findById(Integer employeeId) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Employee result = session.find(Employee.class, employeeId);
        System.out.println(result);
        System.out.println(result.getDepartment());
        result.getProjects().forEach(it -> System.out.println(it));
        session.close();
        return result;
    }

    public List<Employee> findByDepartment(String departmentName) {

        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String selectEmployeesByDept = "from Employee e where e.department.name = :deptName";
        Query selectQuery = session.createQuery(selectEmployeesByDept);
        selectQuery.setParameter("deptName", departmentName);
        List<Employee> allDepartments = selectQuery.getResultList();
        session.close();
        return allDepartments;
    }

    public List<Employee> findByDepartmentByCriteria(String departmentName) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Employee> cr = cb.createQuery(Employee.class);
        Root<Employee> root = cr.from(Employee.class); // from Employee

        cr.select(root); //  select * from Employee e
        cr.where(cb.equal(root.get("department").get("name"), departmentName));  ///  where e.department.name = "departmentName"


        Query<Employee> query = session.createQuery(cr);
        List<Employee> result = query.getResultList();

        session.close();
        return result;

    }

    public void save(Employee employee) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(employee);
        transaction.commit();
        session.close();
    }

    public void delete(Employee employee) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(employee);
        transaction.commit();
        session.close();
    }

    public void update(Employee employee) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(employee);
        transaction.commit();
        session.close();
    }

    public List<Employee> findAll() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String selectAllEmployees = "select e from Employee e";
        List<Employee> result = session.createQuery(selectAllEmployees).getResultList();
        session.close();
        return result;
    }

    public List<Employee> findAllByNameStartsWith(String startingName) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String selectAllEmployees = "select e from Employee e where e.firstName like :startName";
        Query query = session.createQuery(selectAllEmployees);
        query.setParameter("startName", startingName + "%");
        List<Employee> result = query.getResultList();
        session.close();
        return result;

    }

    public List<Employee> findAllByNameStartsWithByCr(String startingName) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Employee> cr = cb.createQuery(Employee.class);
        Root<Employee> root = cr.from(Employee.class); // from Employee

        cr.select(root); //  select * from Employee e

        cr.where(cb.like(root.get("firstName"), startingName + "%"));
        Query<Employee> query = session.createQuery(cr);
        List<Employee> result = query.getResultList();
        session.close();
        return result;

    }




    public List<Employee> findAllSorted(String order) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String selectAllEmployees = "select e from Employee e order by e.lastName " + order;
        Query query = session.createQuery(selectAllEmployees);
        List<Employee> result = query.getResultList();
        session.close();
        return result;
    }


    public List<Employee> findAllSortedByCr() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Employee> cr = cb.createQuery(Employee.class);
        Root<Employee> root = cr.from(Employee.class); // from Employee

        cr.select(root); //  select * from Employee e
        cr.orderBy(cb.desc(root.get("lastName")));
        Query<Employee> query = session.createQuery(cr);
        List<Employee> result = query.getResultList();
        session.close();
        return result;

    }



    public List<Employee> findAllSortedASC() {

        return findAllSorted("asc");
    }

    public List<Employee> findAllSortedDESC() {
        return findAllSorted("desc");
    }

}
