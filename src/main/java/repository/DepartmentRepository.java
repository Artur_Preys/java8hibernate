package repository;

import static utils.DatabaseUtils.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.nimbus.State;

import domain.Department;

public class DepartmentRepository {

    private String selectAllFromDepartments = "SELECT * FROM departments";

    public List<Department> findAll() {

        List<Department> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(selectAllFromDepartments)) {
            while (rs.next()) {
                Integer deptId = rs.getInt("departmentId");
                String deptName = rs.getString("name");
                Department department = new Department(deptId, deptName);
                result.add(department);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return result;
    }


    public void save(Department department){
        String createSql = "INSERT INTO departments(name) values(?)";

        try(Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement(createSql)
        ){
            stmt.setString(1, department.getName());
            stmt.executeUpdate();
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

    }





    public void updateFirstDepartment(String deptName){
        try(Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
            Statement stmt = conn.createStatement()){
            String sql = "update departments set name=" +"'"+  deptName + "'" +" where departmentId = 1";
            int affectedRows = stmt.executeUpdate(sql);
            System.out.println("rows affected: " + affectedRows);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void findByDepartmentName(String deptName){

        try(Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM departments WHERE name = ?")
           ){
            stmt.setString(1, deptName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Integer deptId = rs.getInt("departmentId");
                String dName = rs.getString("name");
                System.out.println(deptId + " " + dName);
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void deleteById(Integer deptId){
        try(Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement("Delete from departments where departmentId = ?")
        ){
            stmt.setInt(1, deptId);
            int affectedRows = stmt.executeUpdate();
            System.out.println("Affected Rows: " + affectedRows);
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

}





