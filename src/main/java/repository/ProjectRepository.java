package repository;

import static utils.DatabaseUtils.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProjectRepository {

    private String selectAllFromProjects = "SELECT * FROM projects";

    public void findAll() {
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(selectAllFromProjects)) {
            while (rs.next()) {
                Integer projectId = rs.getInt("projectId");
                String description = rs.getString("description");
                System.out.println(projectId + " " + description);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void findById(Integer projectId) {
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("SELECT * from projects where projectId = ?")
        ) {
            stmt.setInt(1, projectId);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Integer projId = rs.getInt("projectId");
                String description = rs.getString("description");
                System.out.println(projId + " " + description);
            }
            rs.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
