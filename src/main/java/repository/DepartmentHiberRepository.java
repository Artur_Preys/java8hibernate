package repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import domain.Department;
import utils.HibernateUtils;

public class DepartmentHiberRepository {

    public List<Department> findAll() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String selectAllDepts = "from Department";

        Query selectQuery = session.createQuery(selectAllDepts);
        List<Department> departments = selectQuery.list();
        session.close();
        return departments;
    }

    public List<Department> findAllByCriteria() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Department> cr = cb.createQuery(Department.class);
        Root<Department> root = cr.from(Department.class);

        cr.select(root);

        Query<Department> query = session.createQuery(cr);
        List<Department> result = query.getResultList();

        session.close();

        return result;
    }

    public List<Department> findAllByName(String depName) {

        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Department> cr = cb.createQuery(Department.class);
        Root<Department> root = cr.from(Department.class);

        cr.select(root);// select * from deparment d
        cr.where(cb.equal(root.get("name"), depName)); // where d.name = :depName

        Query<Department> query = session.createQuery(cr);
        List<Department> result = query.getResultList();

        session.close();

        return result;
    }



    public List<Department> findAllByNameOrId(String depName, Integer depId) {

        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Department> cr = cb.createQuery(Department.class);
        Root<Department> root = cr.from(Department.class);


        cr.select(root);// select * from deparment d
        cr.where(
            cb.or(cb.equal(root.get("name"), depName), cb.equal(root.get("departmentId"), depId))
        ); // where d.name = :depName or d.departmentId = :depId


        Query<Department> query = session.createQuery(cr);
        List<Department> result = query.getResultList();

        session.close();

        return result;
    }


}
