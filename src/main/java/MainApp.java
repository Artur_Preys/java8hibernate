import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainApp {

    public static final String DATABASE_HOST = "jdbc:mysql://localhost:3306/jdbcJava8HumanResources?serverTimezone=UTC";
    public static final String DATABASE_USERNAME = "root";
    public static final String DATABASE_PASSWORD = "";

    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
            System.out.println("Auto commit is: " + conn.getAutoCommit());
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM departments");

            while(rs.next()){
                Integer deptId = rs.getInt("departmentId");
                String deptName = rs.getString("name");
                System.out.println(deptId + " " + deptName);
            }

            System.out.println("\nDisplay all projects: ");
            rs = stmt.executeQuery("SELECT * FROM projects");
            while(rs.next()){
                Integer projectId = rs.getInt("projectId");
                String description = rs.getString("description");
                System.out.println(projectId + " " + description);
            }
            System.out.println("\nDisplay all employees: ");
            rs = stmt.executeQuery("SELECT * FROM employees");
            printEmployees(rs);

            System.out.println("\nDisplay all employees with letter J name: ");
            rs = stmt.executeQuery("SELECT * FROM employees where firstName like 'J%'");
            printEmployees(rs);

            System.out.println("\nDisplay all employees that haven’t been assigned to a department ");
            rs = stmt.executeQuery("SELECT * FROM employees where departmentId is null");
            printEmployees(rs);

            System.out.println(" \n Display all employees along with the department they’re in"
                + " (employeeId, firstName, lastName,dateOfBirth, departmentName)");
            rs = stmt.executeQuery("SELECT e.employeeId, e.firstName, e.lastName, e.departmentId, e.dateOfBirth, d.name"
                + "  from employees e JOIN departments d ON e.departmentId = d.departmentId");
            printEmployees(rs);

            rs.close();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void printEmployees(ResultSet rs) throws SQLException{
        while(rs.next()){
            String deparmentName = "";

            Integer employeeId = rs.getInt("employeeId");
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            Date dateOfBirth = rs.getDate("dateOfBirth");
            Integer deparmentId = rs.getInt("departmentId");
            try{
                deparmentName = rs.getString("name");
            }catch (SQLException ex){}
            System.out.println(employeeId + " " + firstName + " " + lastName + " " + dateOfBirth + " " + deparmentId + " " + deparmentName);
        }
    }

}
